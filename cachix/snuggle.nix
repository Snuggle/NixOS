
{
  nix = {
    binaryCaches = [
      "https://snuggle.cachix.org"
    ];
    binaryCachePublicKeys = [
      "snuggle.cachix.org-1:0NGbcJVVAV0lVzqeEGv6LiliMGER3hHn2i3VGw4LMyI="
    ];
  };
}
